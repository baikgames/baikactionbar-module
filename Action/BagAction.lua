-- Load Module
local Baik = _G.Baik
local Class = Baik.Base.Class
local Log = Baik.Base.Log
local ProfileEvent= Baik.Event.ProfileEvent
local RepositoryAction = Baik.Action.RepositoryAction
local Visibility = Baik.Data.Visibility

local BaikActionBar = _G.BaikActionBar

-- Create Module
local BagAction = BaikActionBar:NewModule("BagAction")

BagAction.Zoom = RepositoryAction.New("baik_bag_zoom", false)
BagAction.Visibility = RepositoryAction.New("baik_bag_visibility", Visibility.SHOW)

-- Private Methods
local function _TriggerEvents()
    BagAction.Zoom:TriggerEvent()
    BagAction.Visibility:TriggerEvent()
end

-- Ace Callbacks
function BagAction:OnInitialize()
    Log:i("BagAction OnInitialize")
end

function BagAction:OnEnable()
    _TriggerEvents()
    ProfileEvent:Get():Register(_TriggerEvents)
    Log:i("BagAction OnEnable")
end

function BagAction:OnDisable()
    Log:i("BagAction OnDisable")
end

-- Export Module
BaikActionBar.Action.BagAction = BagAction
