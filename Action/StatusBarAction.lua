-- Load Module
local Baik = _G.Baik
local Class = Baik.Base.Class
local Log = Baik.Base.Log
local ProfileEvent= Baik.Event.ProfileEvent
local RepositoryAction = Baik.Action.RepositoryAction

local BaikActionBar = _G.BaikActionBar

-- Create Module
local StatusBarAction = BaikActionBar:NewModule("StatusBarAction")

StatusBarAction.Xp = RepositoryAction.New("baik_status_bar_xp", true)

-- Private Methods
local function _TriggerEvents()
    StatusBarAction.Xp:TriggerEvent()
end

-- Ace Callbacks
function StatusBarAction:OnInitialize()
    Log:i("StatusBarAction OnInitialize")
end

function StatusBarAction:OnEnable()
    _TriggerEvents()
    ProfileEvent:Get():Register(_TriggerEvents)
    Log:i("StatusBarAction OnEnable")
end

function StatusBarAction:OnDisable()
    Log:i("StatusBarAction OnDisable")
end

-- Export Module
BaikActionBar.Action.StatusBarAction = StatusBarAction
