-- Load Module
local Baik = _G.Baik
local Class = Baik.Base.Class
local Log = Baik.Base.Log
local ProfileEvent= Baik.Event.ProfileEvent
local RepositoryAction = Baik.Action.RepositoryAction
local Visibility = Baik.Data.Visibility

local BaikActionBar = _G.BaikActionBar

-- Create Module
local ActionBarAction = BaikActionBar:NewModule("ActionBarAction")

ActionBarAction.Zoom = RepositoryAction.New("baik_action_bar_zoom", false)
ActionBarAction.Extra = RepositoryAction.New("baik_action_bar_extra", false)
ActionBarAction.Gryphon = RepositoryAction.New("baik_action_bar_gryphon", true)
ActionBarAction.Visibility = RepositoryAction.New("baik_action_bar_visibility", Visibility.SHOW)

-- Private Methods
local function _TriggerEvents()
    ActionBarAction.Zoom:TriggerEvent()
    ActionBarAction.Extra:TriggerEvent()
    ActionBarAction.Gryphon:TriggerEvent()
    ActionBarAction.Visibility:TriggerEvent()
end

-- Public API
function ActionBarAction:HasGrid()
    return ALWAYS_SHOW_MULTIBARS == "1" or ALWAYS_SHOW_MULTIBARS == 1
end

function ActionBarAction:HasBottomLeft()
    return SHOW_MULTI_ACTIONBAR_1 == "1" or SHOW_MULTI_ACTIONBAR_1 == 1
end

function ActionBarAction:HasBottomRight()
    return SHOW_MULTI_ACTIONBAR_2 == "1" or SHOW_MULTI_ACTIONBAR_2 == 1
end

function ActionBarAction:GetNumStances()
    return GetNumShapeshiftForms()
end

function ActionBarAction:GetBottomLeftHeight()
    return 8
end

function ActionBarAction:GetBottomRightHeight()
    return 48
end

-- Ace Callbacks
function ActionBarAction:OnInitialize()
    Log:i("ActionBarAction OnInitialize")
end

function ActionBarAction:OnEnable()
    _TriggerEvents()
    ProfileEvent:Get():Register(_TriggerEvents)
    Log:i("ActionBarAction OnEnable")
end

function ActionBarAction:OnDisable()
    Log:i("ActionBarAction OnDisable")
end

-- Export Module
BaikActionBar.Action.ActionBarAction = ActionBarAction
