-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Event = Baik.Event.Event
local Log = Baik.Base.Log

local BaikActionBar = _G.BaikActionBar

-- Create Module
local SpellBookEvent = Baik:NewModule("SpellBookEvent")

-- Private Variables
local _hide_event = Event.New()
local _show_event = Event.New()

-- Private Methods
local function _TriggerHideEvent()
    _hide_event:Trigger()
end

local function _TriggerShowEvent()
    _show_event:Trigger()
end

-- Public API
function SpellBookEvent:GetHide()
    return _hide_event
end

function SpellBookEvent:GetShow()
    return _show_event
end

-- Ace Callbacks
function SpellBookEvent:OnInitialize()
    Log:i("SpellBookEvent OnInitialize")
end

function SpellBookEvent:OnEnable()
    Baik:SecureHook(SpellBookFrame, "Hide", _TriggerHideEvent)
    Baik:SecureHook(SpellBookFrame, "Show", _TriggerShowEvent)
    Log:i("SpellBookEvent OnEnable")
end

function SpellBookEvent:OnDisable()
    Log:i("SpellBookEvent OnDisable")
end

-- Export Module
BaikActionBar.Event.SpellBookEvent = SpellBookEvent
