-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Event = Baik.Event.Event
local Log = Baik.Base.Log

local BaikActionBar = _G.BaikActionBar

-- Create Module
local ActionBarOptionEvent = Baik:NewModule("ActionBarOptionEvent")

-- Private Variables
local _actionbar_event = Event.New()

-- Private Methods
local function _TriggerOptionEvent()
    _actionbar_event:Trigger()
end

-- Public API
function ActionBarOptionEvent:Get()
    return _actionbar_event
end

-- Ace Callbacks
function ActionBarOptionEvent:OnInitialize()
    Log:i("ActionBarOptionEvent OnInitialize")
end

function ActionBarOptionEvent:OnEnable()
    Log:i("ActionBarOptionEvent OnEnable")
    Baik:SecureHook("SetActionBarToggles", _TriggerOptionEvent)
end

function ActionBarOptionEvent:OnDisable()
    Log:i("ActionBarOptionEvent OnDisable")
end

-- Export Module
BaikActionBar.Event.ActionBarOptionEvent = ActionBarOptionEvent
