-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Event = Baik.Event.Event
local Log = Baik.Base.Log

local BaikActionBar = _G.BaikActionBar

-- Create Module
local StatusBarEvent = Class()

-- Public API
StatusBarEvent.Visibility = Event.New()

-- Export Module
BaikActionBar.Event.StatusBarEvent = StatusBarEvent
