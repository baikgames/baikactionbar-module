-- Load Modules
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Log = Baik.Base.Log
local Table = Baik.Base.Table

local BaikActionBar = _G.BaikActionBar
local XpStatusBarTracker = BaikActionBar.Frame.XpStatusBarTracker
local StatusBarAction = BaikActionBar.Action.StatusBarAction
local StatusBarEvent = BaikActionBar.Event.StatusBarEvent
local StatusBarFrame = BaikActionBar.Frame.StatusBarFrame

-- Create Module
local StatusBarController = BaikActionBar:NewModule("StatusBarController")

-- Constants
local _EVENTS = {
    StatusBarAction.Xp:Event()
}

local _TRACKERS = {
    XpStatusBarTracker,
}

-- Private Variables
local _cur_tracker = nil

-- Private Methods
local function _SetupTracker()
    -- Find next tracker
    local new_tracker = nil
    Table.ForEach(_TRACKERS, function(tracker)
        if tracker:IsValid() then
            new_tracker = tracker
            return true
        end
    end)

    -- Current Tracker
    if new_tracker == _cur_tracker then
        return
    end

    -- Deactivate Tracker
    if _cur_tracker ~= nil then
        _cur_tracker:UnregisterEvents()
        StatusBarFrame:Hide()
        StatusBarEvent.Visibility:Trigger(false)
    end

    if new_tracker ~= nil then
        new_tracker:RegisterEvents()
        new_tracker:Setup()
        new_tracker:Update()
        StatusBarFrame:Show()
        StatusBarEvent.Visibility:Trigger(true)
    end

    _cur_tracker = new_tracker
end

local function _RegisterEvents()
    Table.ForEach(_EVENTS, function(event)
        event:Register(_SetupTracker)
    end)
end

-- Ace Callbacks
function StatusBarController:OnInitialize()
    Log:i("StatusBarController OnInitialize")
end

function StatusBarController:OnEnable()
    _RegisterEvents()
    _SetupTracker()
    -- Hide frame if not tracking anything
    if _cur_tracker == nil then
        StatusBarFrame:Hide()
    end
    Log:i("StatusBarController OnEnable")
end

function StatusBarController:OnDisable()
    Log:i("StatusBarController OnDisable")
end
