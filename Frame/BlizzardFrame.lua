-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local HideFrame = Baik.Frame.HideFrame
local Log = Baik.Base.Log
local Table = Baik.Base.Table

local BaikActionBar = _G.BaikActionBar

-- Create Module
local BlizzardFrame = BaikActionBar:NewModule("BlizzardFrame")

-- Constants
local _HIDE_FRAMES = {
    MultiBarBottomLeft,
    MultiBarBottomRight,
    MultiCastActionBarFrame,
    PossessBarFrame,
    PetActionBarFrame,
    StanceBarFrame
}

local _ALPHA_FRAMES = {
    MainMenuBarArtFrame,
    StanceButton1NormalTexture2,
    StanceButton2NormalTexture2,
    StanceButton3NormalTexture2,
    StanceButton4NormalTexture2,
    StanceButton5NormalTexture2,
    StanceButton6NormalTexture2,
    StanceButton7NormalTexture2,
    StanceButton8NormalTexture2,
    StanceButton9NormalTexture2,
    StanceButton10NormalTexture2
}

-- Ace Callbacks
function BlizzardFrame:OnInitialize()
    Log:i("BlizzardFrame OnInitialize")
    Table.ForEach(_HIDE_FRAMES, function(frame)
        HideFrame:Add(frame)
    end)

    Table.ForEach(_ALPHA_FRAMES, function(frame)
        frame:SetAlpha(0.0)
    end)

    StatusTrackingBarManager:EnableMouse(false)
    StatusTrackingBarManager:UnregisterAllEvents()
    StatusTrackingBarManager:Hide()

    MainMenuBar:EnableMouse(false)
    MainMenuBar:SetAlpha(0)
    MainMenuBar:UnregisterEvent("ACTIONBAR_PAGE_CHANGED")
    MainMenuBar:UnregisterEvent("PLAYER_ENTERING_WORLD")
    MainMenuBar:UnregisterEvent("DISPLAY_SIZE_CHANGED")
    MainMenuBar:UnregisterEvent("UI_SCALE_CHANGED")
end

function BlizzardFrame:OnEnable()
    Log:i("BlizzardFrame OnEnable")
end

function BlizzardFrame:OnDisable()
    Log:i("BlizzardFrame OnDisable")
end

-- Export Module
BaikActionBar.Frame.BlizzardFrame = BlizzardFrame
