-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local CombatSafe = Baik.Base.CombatSafe
local Log = Baik.Base.Log
local Table = Baik.Base.Table

local BaikActionBar = _G.BaikActionBar
local ActionBarAction = BaikActionBar.Action.ActionBarAction
local ActionBarOptionEvent = BaikActionBar.Event.ActionBarOptionEvent

-- Create Module
local VehicleActionBar = BaikActionBar:NewModule("VehicleActionBar")

-- Constants
local _HEIGHT = { 0, 40, 80 }

-- Private API
local function _Update()
    if not CanExitVehicle() then
        return
    end

    local count = 1
    local count_checks = {
        ActionBarAction:HasBottomLeft(),
        ActionBarAction:HasBottomRight()
    }
    Table.ForEach(count_checks, function(value)
        if value then
            count = count + 1
        end
    end)
    local height = _HEIGHT[count]
    MainMenuBarVehicleLeaveButton:ClearAllPoints()
    MainMenuBarVehicleLeaveButton:SetPoint("BOTTOMRIGHT", BaikArtFrame, "TOPRIGHT", -48, height)
    MainMenuBarVehicleLeaveButton:Show()
end

local function _CreateBarFrame()
    local frame = CreateFrame("Frame",
                              "BaikVehicleActionBar",
                              BaikArtFrame,
                              "SecureFrameTemplate")
    frame:SetFrameStrata("MEDIUM")
    frame:SetToplevel(true)
    frame:EnableMouse(true)

    MainMenuBarVehicleLeaveButton:SetParent(frame)
    MainMenuBarVehicleLeaveButton:SetSize(30, 30)

    return frame
end

-- Private API
function VehicleActionBar:_SetupEvents()
    Baik:SecureHook("MainMenuBarVehicleLeaveButton_Update", _Update)
    ActionBarOptionEvent:Get():Register(CombatSafe:CallbackFunc(_Update))
end

-- Ace Callbacks
function VehicleActionBar:OnInitialize()
    self._frame = _CreateBarFrame()
    self:_SetupEvents()
    Log:i("VehicleActionBar OnInitialize")
end

function VehicleActionBar:OnEnable()
    Log:i("VehicleActionBar OnEnable")
end

function VehicleActionBar:OnDisable()
    Log:i("VehicleActionBar OnDisable")
end

-- Export Module
BaikActionBar.Frame.VehicleActionBar = VehicleActionBar
