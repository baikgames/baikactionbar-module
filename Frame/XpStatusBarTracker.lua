-- Load Modules
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Color = Baik.Base.Color
local GameEvent = Baik.Event.GameEvent
local PlayerAction = Baik.Action.PlayerAction
local Table = Baik.Base.Table

local BaikActionBar = _G.BaikActionBar
local StatusBarAction = BaikActionBar.Action.StatusBarAction
local StatusBarFrame = BaikActionBar.Frame.StatusBarFrame
local StatusBarTracker = BaikActionBar.Frame.StatusBarTracker

-- Constants
local _XP_COLOR = Color.New(0.58, 0.0, 0.55, 1.0)
local _RESTED_COLOR = Color.New(0.0, 0.39, 0.88, 1.0)
local _EVENTS = {
    GameEvent:Get("PLAYER_ENTERING_WORLD"),
    GameEvent:Get("PLAYER_UPDATE_RESTING"),
    GameEvent:Get("PLAYER_XP_UPDATE"),
    GameEvent:Get("UPDATE_EXHAUSTION"),
}

-- Create Module
local XpStatusBarTracker = Class(StatusBarTracker)

-- Public API
function XpStatusBarTracker:Setup()
    StatusBarFrame:SetColor(_XP_COLOR, _RESTED_COLOR)
end

function XpStatusBarTracker:RegisterEvents()
    Table.ForEach(_EVENTS, function(event)
        event:Register(self, XpStatusBarTracker.Update)
    end)
end

function XpStatusBarTracker:UnregisterEvents()
    Table.ForEach(_EVENTS, function(event)
        event:Unregister(self)
    end)
end

function XpStatusBarTracker:Update()
    local cur_xp = PlayerAction:GetXp()
    local max_xp = PlayerAction:GetXpMax()
    local exhaust_xp = PlayerAction:GetXpExhaustion()
    local cur_exhaust_xp = exhaust_xp and math.min(exhaust_xp + cur_xp, max_xp) or 0
    StatusBarFrame:SetBar(cur_xp, max_xp, cur_exhaust_xp, max_xp)
end

function XpStatusBarTracker:IsValid()
    local enabled = StatusBarAction.Xp:Get()
    local level = PlayerAction:GetLevel()
    local max_level = PlayerAction:GetMaxLevel()

    return enabled and level < max_level
end

-- Export Module
BaikActionBar.Frame.XpStatusBarTracker = XpStatusBarTracker
