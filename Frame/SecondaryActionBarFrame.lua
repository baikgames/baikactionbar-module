-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local GameEvent = Baik.Event.GameEvent
local Table = Baik.Base.Table

local BaikActionBar = _G.BaikActionBar
local ActionBarAction = BaikActionBar.Action.ActionBarAction
local ActionBarOptionEvent = BaikActionBar.Event.ActionBarOptionEvent

-- Create Module
local SecondaryActionBarFrame = Class()

-- Constants
local _HEIGHT = { 4, 44, 84 }

-- Private API
function SecondaryActionBarFrame:_Update()
    if InCombatLockdown() then
        return
    end

    local count = 1
    local count_checks = {
        ActionBarAction:HasBottomLeft(),
        ActionBarAction:HasBottomRight()
    }
    Table.ForEach(count_checks, function(value)
        if value then
            count = count + 1
        end
    end)

    local frame = self._frame
    local height = _HEIGHT[count]
    frame:ClearAllPoints()
    frame:SetPoint("BOTTOM", BaikMainActionBarFrame, "TOP", 0, height)
end

-- Public API
function SecondaryActionBarFrame:Get()
    return self._frame
end

function SecondaryActionBarFrame:SetupEvents()
    ActionBarOptionEvent:Get():Register(self, SecondaryActionBarFrame._Update)
    GameEvent:Get("PLAYER_REGEN_ENABLED"):Register(self, SecondaryActionBarFrame._Update)
end

function SecondaryActionBarFrame.New(name, parent)
    Assert.String(name)

    local obj = Class(SecondaryActionBarFrame)
    local frame = CreateFrame("Frame",
                              name,
                              parent,
                              "SecureHandlerStateTemplate")
    frame:SetFrameStrata("MEDIUM")
    frame:SetSize(400, 38)
    frame:SetToplevel(true)
    frame:EnableMouse(true)

    obj._frame = frame

    return obj
end

-- Export Module
BaikActionBar.Frame.SecondaryActionBarFrame = SecondaryActionBarFrame
