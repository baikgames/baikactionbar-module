-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Log = Baik.Base.Log

local BaikActionBar = _G.BaikActionBar
local ActionBar = BaikActionBar.Frame.ActionBar
local ActionBarFrame = BaikActionBar.Frame.ActionBarFrame

-- Create Module
local MainActionBar = BaikActionBar:NewModule("MainActionBar")

-- Constants
local _FORMAT = "ActionButton%d"
local _PAGE_NUMBER = 1
local _STATES = [=[
    [shapeshift] 1;
    [vehicleui] 2;
    [possessbar] 3;
    [overridebar] 4;
    [bonusbar:0] 10;
    [bonusbar:1] 11;
    [bonusbar:2] 12;
    [bonusbar:3] 13;
    [bonusbar:4] 14;
    [bonusbar:5] 15;
    0
]=]

-- Private API
local function _CreateBarFrame()
    local frame = ActionBarFrame.New("BaikMainActionBarFrame",
                                     BaikArtFrame,
                                     _PAGE_NUMBER)
    frame:SetPoint("BOTTOMLEFT", 8, 3)
    frame:SetAttribute("currentpage", 1)
    frame:SetAttribute("_onstate-overrides", string.format(
    [=[
        local NUM_ACTIONBAR_PAGES = %d
        local page = nil
        if newstate == 1 then
            page = GetTempShapeshiftBarIndex()
        elseif newstate == 2 or newstate == 3 then
            page = GetVehicleBarIndex()
        elseif newstate == 4 then
            page = GetOverrideBarIndex()
        else
            local offset = GetBonusBarOffset()
            local cur_page = self:GetAttribute("currentpage")
            page = offset == 0 and cur_page or NUM_ACTIONBAR_PAGES + offset
        end

        self:SetAttribute("actionpage", page)
        self:ChildUpdate("page", page)
    ]=], NUM_ACTIONBAR_PAGES))

    RegisterStateDriver(frame, "overrides", _STATES)
    return frame
end

local function _CreatePageUpButton(parent)
    local button = CreateFrame("BUTTON",
                               "BaikPageUp",
                               parent,
                               "SecureHandlerClickTemplate")
    button:SetSize(21, 19)
    button:SetPoint("RIGHT", ActionButton12, 25, 9)
    button:SetNormalAtlas("hud-MainMenuBar-arrowup-up")
    button:SetPushedAtlas("hud-MainMenuBar-arrowup-down")
    button:SetDisabledAtlas("hud-MainMenuBar-arrowup-disabled")
    button:SetHighlightAtlas("hud-MainMenuBar-arrowup-highlight", "ADD")
    button:SetAttribute("_onclick",
    [=[
        local parent = self:GetParent()
        local cur_page = parent:GetAttribute("currentpage") + 1
        if cur_page > 2 then
            cur_page = 1
        end
        parent:SetAttribute("currentpage", cur_page)
        parent:SetAttribute("actionpage", cur_page)
        parent:ChildUpdate("page", cur_page)
    ]=])

    return button
end

local function _CreatePageDownButton(parent)
    local button = CreateFrame("BUTTON",
                               "BaikPageUp",
                               parent,
                               "SecureHandlerClickTemplate")
    button:SetSize(21, 19)
    button:SetPoint("RIGHT", ActionButton12, 25, -11)
    button:SetNormalAtlas("hud-MainMenuBar-arrowdown-up")
    button:SetPushedAtlas("hud-MainMenuBar-arrowdown-down")
    button:SetDisabledAtlas("hud-MainMenuBar-arrowdown-disabled")
    button:SetHighlightAtlas("hud-MainMenuBar-arrowdown-highlight", "ADD")
    button:SetAttribute("_onclick",
    [=[
        local parent = self:GetParent()
        local cur_page = parent:GetAttribute("currentpage") - 1
        if cur_page < 1 then
            cur_page = 2
        end
        parent:SetAttribute("currentpage", cur_page)
        parent:SetAttribute("actionpage", cur_page)
        parent:ChildUpdate("page", cur_page)
    ]=])

    return button
end

-- Ace Callbacks
function MainActionBar:OnInitialize()
    local frame = _CreateBarFrame()
    self._action_bar = ActionBar:New(frame, _FORMAT, NUM_MULTIBAR_BUTTONS)
    self._page_up = _CreatePageUpButton(frame)
    self._page_down = _CreatePageDownButton(frame)
    self._frame = frame
    self._action_bar:SetupEvents()
    Log:i("MainActionBar OnInitialize")
end

function MainActionBar:OnEnable()
    Log:i("MainActionBar OnEnable")
end

function MainActionBar:OnDisable()
    Log:i("MainActionBar OnDisable")
end

-- Export Module
BaikActionBar.Frame.MainActionBar = MainActionBar
