-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Frame = Baik.Base.Frame
local Log = Baik.Base.Log
local Table = Baik.Base.Table
local ZoomAction = Baik.Action.ZoomAction

local BaikActionBar = _G.BaikActionBar
local ActionBarAction = BaikActionBar.Action.ActionBarAction

-- Constants
local _BUTTONS = Frame.FromPattern("MultiBarLeftButton%d",
                                   NUM_MULTIBAR_BUTTONS)

-- Create Module
local LeftActionBar = BaikActionBar:NewModule("LeftActionBar")

-- Private Methods
local function _Zoom(zoom)
    Table.ForEach(_BUTTONS, function(button)
        ZoomAction.Button(button, zoom)
    end)
end

local function _ExtraButton(extra)
    local _DEFAULT = {
        parent = MultiBarLeft,
        args = { "TOP", MultiBarLeftButton11, "BOTTOM", 0, -6 }
    }
    local _EXTRA = {
        parent = BaikBottomLeftActionBarFrame,
        args = { "LEFT", MultiBarBottomLeftButton12, "RIGHT", 6, 0 }
    }

    local value = extra and _EXTRA or _DEFAULT
    MultiBarLeftButton12:SetParent(value.parent)
    MultiBarLeftButton12:ClearAllPoints()
    MultiBarLeftButton12:SetPoint(unpack(value.args))
end

-- Ace Callbacks
function LeftActionBar:OnInitialize()
    MultiBarLeftButton12:SetAttribute("actionpage", 4)
    ActionBarAction.Zoom:Event():Register(_Zoom)
    ActionBarAction.Extra:Event():Register(_ExtraButton)
    Log:i("LeftActionBar OnInitialize")
end

function LeftActionBar:OnEnable()
    Log:i("LeftActionBar OnEnable")
end

function LeftActionBar:OnDisable()
    Log:i("LeftActionBar OnDisable")
end
-- Export Module
BaikActionBar.Frame.LeftActionBar = LeftActionBar
