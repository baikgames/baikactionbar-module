-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Log = Baik.Base.Log

local BaikActionBar = _G.BaikActionBar
local ActionBarAction = BaikActionBar.Action.ActionBarAction
local ActionBarFrame = BaikActionBar.Frame.ActionBarFrame
local ActionBarOptionEvent = BaikActionBar.Event.ActionBarOptionEvent
local ActionBar = BaikActionBar.Frame.ActionBar

-- Create Module
local BottomLeftActionBar = BaikActionBar:NewModule("BottomLeftActionBar")

-- Constants
local _FORMAT = "MultiBarBottomLeftButton%d"
local _PAGE_NUMBER = 6
local _BOTTOM_LEFT_HEIGHT = ActionBarAction:GetBottomLeftHeight()

-- Private Methods
local function _CreateBarFrame()
    local frame = ActionBarFrame.New("BaikBottomLeftActionBarFrame",
                                     BaikArtFrame,
                                     _PAGE_NUMBER)
    frame:SetPoint("BOTTOM",
                   BaikMainActionBarFrame,
                   "TOP",
                   0,
                   _BOTTOM_LEFT_HEIGHT)

    return frame
end

-- Private API
function BottomLeftActionBar:_Update()
    if InCombatLockdown() then
        return
    end

    local frame = self._frame
    local has_bottom_left = ActionBarAction:HasBottomLeft()
    if not has_bottom_left then
        frame:Hide()
        return
    end

    frame:Show()
end

-- Ace Callbacks
function BottomLeftActionBar:OnInitialize()
    local frame = _CreateBarFrame()
    self._action_bar = ActionBar:New(frame, _FORMAT, NUM_MULTIBAR_BUTTONS)
    self._frame = frame
    self._action_bar:SetupEvents()
    ActionBarOptionEvent:Get():Register(self, BottomLeftActionBar._Update)
    Log:i("BottomLeftActionBar OnInitialize")
end

function BottomLeftActionBar:OnEnable()
    Log:i("BottomLeftActionBar OnEnable")
end

function BottomLeftActionBar:OnDisable()
    Log:i("BottomLeftActionBar OnDisable")
end

-- Export Module
BaikActionBar.Frame.BottomLeftActionBar = BottomLeftActionBar
