-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Log = Baik.Base.Log

local BaikActionBar = _G.BaikActionBar
local ActionBarAction = BaikActionBar.Action.ActionBarAction
local ActionBar = BaikActionBar.Frame.ActionBar
local SecondaryActionBarFrame = BaikActionBar.Frame.SecondaryActionBarFrame

-- Create Module
local StanceActionBar = BaikActionBar:NewModule("StanceActionBar")

-- Constants
local _FORMAT = "StanceButton%d"

-- Private API
local function _CreateBarFrame()
    local secondary_bar = SecondaryActionBarFrame.New("BaikStanceActionBarFrame",
                                                      BaikArtFrame)
    local frame = secondary_bar:Get()
    frame:SetAttribute("hasstance", false)
    frame:SetAttribute("_onstate-stance",
    [=[
        local has_stance = self:GetAttribute("hasstance")
        if not has_stance then
            return
        end

        local show = newstate == "show"
        if show then
            self:Show()
        else
            self:Hide()
        end
    ]=])
    RegisterStateDriver(frame, "stance", "[nopossessbar, novehicleui] show; hide")

    return secondary_bar
end

-- Private API
function StanceActionBar:_Setup()
    local frame = self._frame:Get()
    local has_stance = ActionBarAction:GetNumStances() > 0
    if not has_stance then
        frame:Hide()
    end
    frame:SetAttribute("hasstance", has_stance)
end

-- Ace Callbacks
function StanceActionBar:OnInitialize()
    local frame = _CreateBarFrame()
    self._action_bar = ActionBar:New(frame:Get(), _FORMAT, NUM_STANCE_SLOTS)
    self._frame = frame
    self._frame:SetupEvents()
    self._action_bar:SetupEvents()
    Log:i("StanceActionBar OnInitialize")
end

function StanceActionBar:OnEnable()
    self:_Setup()
    Log:i("StanceActionBar OnEnable")
end

function StanceActionBar:OnDisable()
    Log:i("StanceActionBar OnDisable")
end

-- Export Module
BaikActionBar.Frame.StanceActionBar = StanceActionBar
