-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class

local BaikActionBar = _G.BaikActionBar

-- Create Module
local ActionBarFrame = Class()

-- Public API
function ActionBarFrame.New(name, parent, action_page)
    Assert.String(name)

    local frame = CreateFrame("Frame",
                              name,
                              parent,
                              "SecureHandlerStateTemplate")
    frame:SetFrameStrata("MEDIUM")
    frame:SetSize(500, 38)
    if action_page ~= nil then
        frame:SetAttribute("actionpage", action_page)
    end
    frame:SetToplevel(true)
    frame:EnableMouse(true)

    return frame
end

-- Export Module
BaikActionBar.Frame.ActionBarFrame = ActionBarFrame
