-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Log = Baik.Base.Log

local BaikActionBar = _G.BaikActionBar
local ActionBarAction = BaikActionBar.Action.ActionBarAction
local ActionBar = BaikActionBar.Frame.ActionBar
local SecondaryActionBarFrame = BaikActionBar.Frame.SecondaryActionBarFrame

-- Create Module
local PossessActionBar = BaikActionBar:NewModule("PossessActionBar")

-- Constants
local _FORMAT = "PossessButton%d"

-- Private API
local function _CreateBarFrame()
    local secondary_bar = SecondaryActionBarFrame.New("BaikPossessActionBarFrame",
                                                      BaikArtFrame)
    local frame = secondary_bar:Get()
    frame:SetPoint("BOTTOM", BaikMainActionBarFrame, "TOP", 4, 84)
    frame:SetAttribute("_onstate-possess",
    [=[
        local show = newstate == "show"
        if show then
            self:Show()
        else
            self:Hide()
        end
    ]=])
    RegisterStateDriver(frame, "possess", "[possessbar, novehicleui, nochanneling:Mind Control] show; hide")

    return secondary_bar
end

-- Ace Callbacks
function PossessActionBar:OnInitialize()
    local frame = _CreateBarFrame()
    self._action_bar = ActionBar:New(frame:Get(), _FORMAT, NUM_POSSESS_SLOTS)
    self._frame = frame
    self._frame:SetupEvents()
    self._action_bar:SetupEvents()
    Log:i("PossessActionBar OnInitialize")
end

function PossessActionBar:OnEnable()
    Log:i("PossessActionBar OnEnable")
end

function PossessActionBar:OnDisable()
    Log:i("PossessActionBar OnDisable")
end

-- Export Module
BaikActionBar.Frame.PossessActionBar = PossessActionBar

