-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local CVarEvent = Baik.Event.CVarEvent
local GameEvent = Baik.Event.GameEvent
local Log = Baik.Base.Log
local Table = Baik.Base.Table

local BaikActionBar = _G.BaikActionBar
local ActionBarAction = BaikActionBar.Action.ActionBarAction
local ActionBarFrame = BaikActionBar.Frame.ActionBarFrame
local ActionBarOptionEvent = BaikActionBar.Event.ActionBarOptionEvent
local GridActionBar = BaikActionBar.Frame.GridActionBar
local SpellBookEvent = BaikActionBar.Event.SpellBookEvent

-- Create Module
local BottomRightActionBar = BaikActionBar:NewModule("BottomRightActionBar")

-- Constants
local _FORMAT = "MultiBarBottomRightButton%d"
local _PAGE_NUMBER = 5
local _SPELLBOOK_REASON = 1
local _EVENT_REASON = 2
local _OPTION_REASON = 3
local _BOTTOM_LEFT_HEIGHT = ActionBarAction:GetBottomLeftHeight()
local _BOTTOM_RIGHT_HEIGHT = ActionBarAction:GetBottomRightHeight()

-- Private Variables
local _grid_state = {}

-- Private Functions
local function _CreateActionBar(frame)
    local action_bar = GridActionBar:New(frame, _FORMAT, NUM_MULTIBAR_BUTTONS)

    Table.ForEach(action_bar._buttons, function(button)
        local name = button:GetName()
        local texture_name = string.format("%sFloatingBG", name)
        local texture = _G[texture_name]
        if texture then
            return
        end

        local texture = button:CreateTexture(texture_name, "BACKGROUND", nil, -1)
        texture:SetTexture("Interface\\Buttons\\UI-Quickslot")
        texture:SetAlpha(0.4)
        texture:SetPoint("TOPLEFT", -15, 15)
        texture:SetPoint("BOTTOMRIGHT", 15, -15)
        button.noGrid = nil
    end)

    return action_bar
end

local function _CreateBarFrame()
    local frame = ActionBarFrame.New("BaikBottomRightActionBarFrame",
                                     BaikArtFrame,
                                     _PAGE_NUMBER)
    frame:SetPoint("BOTTOM", BaikMainActionBarFrame, "TOP", 0, 48)

    return frame
end

local function _ShowGrid(action_bar, reason)
    _grid_state[reason] = true
    action_bar:ShowGrid()
end

local function _HideGrid(action_bar, reason)
    _grid_state[reason] = false

    local hide_state = true
    for key, value in pairs(_grid_state) do
        if value == true then
            return
        end
    end

    action_bar:HideGrid()
end

local function _UpdateGrid(action_bar)
    for key, value in pairs(_grid_state) do
        if value == true then
            action_bar:ShowGrid()
            return
        end
    end

    action_bar:HideGrid()
end

-- Private API
function BottomRightActionBar:_UpdateGridOption()
    _grid_state[_OPTION_REASON] = ActionBarAction:HasGrid()
end

function BottomRightActionBar:_Update()
    if InCombatLockdown() then
        return
    end

    local frame = self._frame
    local has_bottom_right = ActionBarAction:HasBottomRight()
    if not has_bottom_right then
        frame:Hide()
        return
    end

    frame:Show()
    local has_bottom_left = ActionBarAction:HasBottomLeft()
    local offset = has_bottom_left and _BOTTOM_RIGHT_HEIGHT
                                   or  _BOTTOM_LEFT_HEIGHT
    frame:ClearAllPoints()
    frame:SetPoint("BOTTOM", BaikMainActionBarFrame, "TOP", 0, offset)
end

function BottomRightActionBar:_SetupEvents()
    local action_bar = self._action_bar
    SpellBookEvent:GetShow():Register(function(...)
        _ShowGrid(action_bar, _SPELLBOOK_REASON)
    end)
    SpellBookEvent:GetHide():Register(function(...)
        _HideGrid(action_bar, _SPELLBOOK_REASON)
    end)
    GameEvent:Get("ACTIONBAR_SHOWGRID"):Register(function(...)
        _ShowGrid(action_bar, _EVENT_REASON)
    end)
    GameEvent:Get("ACTIONBAR_HIDEGRID"):Register(function(...)
        _HideGrid(action_bar, _EVENT_REASON)
    end)
    GameEvent:Get("PLAYER_REGEN_ENABLED"):Register(function(...)
        _UpdateGrid(action_bar)
        self:_Update()
    end)
    CVarEvent:Get("ALWAYS_SHOW_MULTIBARS_TEXT"):Register(function(value)
        if value == "1" then
            _ShowGrid(action_bar, _OPTION_REASON)
        else
            _HideGrid(action_bar, _OPTION_REASON)
        end
    end)
    ActionBarOptionEvent:Get():Register(self, BottomRightActionBar._Update)
end

-- Ace Callbacks
function BottomRightActionBar:OnInitialize()
    -- Create member variables
    local frame = _CreateBarFrame()
    local action_bar = _CreateActionBar(frame)

    self._frame = frame
    self._action_bar = action_bar
    self:_SetupEvents()
    self._action_bar:SetupEvents()
    Log:i("BottomRightActionBar OnInitialize")
end

function BottomRightActionBar:OnEnable()
    -- Setup Frame
    self:_UpdateGridOption()
    _UpdateGrid(self._action_bar)

    Log:i("BottomRightActionBar OnEnable")
end

function BottomRightActionBar:OnDisable()
    Log:i("BottomRightActionBar OnDisable")
end

-- Export Module
BaikActionBar.Frame.BottomRightActionBar = BottomRightActionBar
