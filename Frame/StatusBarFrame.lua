-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Color = Baik.Base.Color
local Log = Baik.Base.Log
local Type = Baik.Base.Type

local BaikActionBar = _G.BaikActionBar
local StatusBarAction = BaikActionBar.Action.StatusBarAction

-- Create Module
local StatusBarFrame = BaikActionBar:NewModule("StatusBarFrame")

-- Constants
local _BUTTON_SPACE = 6

-- Private API
local function _CreateStatusBar(name, parent)
    local status_bar_name = string.format("%sStatusBar", name)
    local status_bar = CreateFrame("StatusBar",
                                   status_bar_name,
                                   parent)
    status_bar:SetFrameStrata("LOW")
    status_bar:SetSize(550, 10)
    status_bar:EnableDrawLayer("BORDER")
    status_bar:SetPoint("RIGHT")
    status_bar:SetStatusBarTexture("Interface\\TargetingFrame\\UI-StatusBar")
    status_bar:SetStatusBarColor(0.0, 0.0, 0.0, 0.9)

    return status_bar
end

local function _CreateOverlayFrame(name, parent)
    local overlay_name = string.format("%sOverlayFrame", name)
    local overlay = CreateFrame("Frame",
                                overlay_name,
                                parent)
    overlay:SetFrameStrata("MEDIUM")
    overlay:SetAllPoints()

    local text_name = string.format("%sText", name)
    local text = overlay:CreateFontString(text_name,
                                          "ARTWORK",
                                          "TextStatusBarText")
    text:SetPoint("CENTER")
    overlay.text = text

    return overlay
end

local function _CreateFrame()
    local name = "BaikStatusBar"
    -- Setup main frame
    local frame = CreateFrame("Frame",
                              name,
                              BaikArtFrame,
                              "SecureHandlerStateTemplate")
    frame:SetFrameStrata("MEDIUM")
    frame:SetSize(550, 11)
    frame:SetToplevel(true)
    frame:EnableMouse(true)
    frame:SetPoint("BOTTOM", UIParent, 18, 0)

    -- Setup background texture
    local texture_name = string.format("%sSingleBarSmall", name)
    local texture = frame:CreateTexture(texture_name, "OVERLAY")
    texture:SetAtlas("hud-MainMenuBar-experiencebar-small-single", true)
    texture:SetPoint("CENTER")
    frame.texture = texture

    -- Setup Status Bar
    local status_bar = _CreateStatusBar(name, frame)
    status_bar:SetFrameLevel(2)
    frame.status_bar = status_bar

    -- Setup Second Status Bar
    local second_name = string.format("%sSecond", name)
    local second_status_bar = _CreateStatusBar(second_name, frame)
    second_status_bar:SetFrameLevel(1)
    frame.second_status_bar = second_status_bar
    frame.second_status_bar:Hide()

    -- Setup Status Bar Background
    local background_name = string.format("%sBackground", name)
    local background_status_bar = _CreateStatusBar(background_name, frame)
    background_status_bar:SetFrameLevel(0)
    frame.background_status_bar = background_status_bar

    -- Setup Overlay Frame
    local overlay = _CreateOverlayFrame(name, frame)
    frame.overlay = overlay

    return frame
end

-- Public API
function StatusBarFrame:SetBar(curr, max, curr_second, max_second)
    Assert.Number(curr)
    Assert.Number(max)

    local frame = self._frame
    local status_bar = frame.status_bar
    status_bar:SetMinMaxValues(0, max)
    status_bar:SetValue(curr)

    local text_frame = frame.overlay.text
    local text = XP_STATUS_BAR_TEXT:format(curr, max)
    text_frame:SetText(text)

    if not Type.IsNumber(curr_second) or not Type.IsNumber(max_second) then
        return
    end

    local second_status_bar = frame.second_status_bar
    second_status_bar:SetMinMaxValues(0, max_second)
    second_status_bar:SetValue(curr_second)

    local delta = curr_second - curr
    if delta > 0 then
        text = string.format("%s (%d)", text, delta)
        text_frame:SetText(text)
    end
end

function StatusBarFrame:SetColor(status_bar_color, second_status_bar_color)
    Assert.Table(status_bar_color)
    Assert.Table(second_status_bar_color)

    local frame = self._frame
    frame.status_bar:SetStatusBarColor(unpack(status_bar_color))

    local second_status_bar = frame.second_status_bar
    if not second_status_bar_color then
        second_status_bar:Hide()
        return
    end
    second_status_bar:SetStatusBarColor(unpack(second_status_bar_color))
    second_status_bar:Show()
end

function StatusBarFrame:Show()
    self._frame:Show()
end

function StatusBarFrame:Hide()
    self._frame:Hide()
end

-- Ace Callbacks
function StatusBarFrame:OnInitialize()
    self._frame = _CreateFrame()
    Log:i("StatusBarFrame OnInitialize")
end

function StatusBarFrame:OnEnable()
    Log:i("StatusBarFrame OnEnable")
end

function StatusBarFrame:OnDisable()
    Log:i("StatusBarFrame OnDisable")
end

-- Export Module
BaikActionBar.Frame.StatusBarFrame = StatusBarFrame
