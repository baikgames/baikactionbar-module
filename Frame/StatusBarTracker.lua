-- Load Modules
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local NotImplementedError = Baik.Error.NotImplementedError

local BaikActionBar = _G.BaikActionBar

-- Create Module
local StatusBarTracker = Class()

-- Public API
function StatusBarTracker:Setup()
    NotImplementedError()
end

function StatusBarTracker:RegisterEvents()
    NotImplementedError()
end

function StatusBarTracker:UnregisterEvents()
    NotImplementedError()
end

function StatusBarTracker:Update()
    NotImplementedError()
end

function StatusBarTracker:IsValid()
    NotImplementedError()
end

-- Export Module
BaikActionBar.Frame.StatusBarTracker = StatusBarTracker
