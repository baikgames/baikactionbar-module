-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local CombatShowFrame = Baik.Frame.CombatShowFrame
local HideFrame = Baik.Frame.HideFrame
local HoverAction = Baik.Action.HoverAction
local Log = Baik.Base.Log
local Table = Baik.Base.Table
local Visibility = Baik.Data.Visibility
local ZoomAction = Baik.Action.ZoomAction

local BaikActionBar = _G.BaikActionBar
local BagAction = BaikActionBar.Action.BagAction

-- Create Module
local MicroButtonAndBagActionBar = BaikActionBar:NewModule("MicroButtonAndBagActionBar")

-- Constants
local _BAG_BUTTONS = {
    CharacterBag0Slot,
    CharacterBag1Slot,
    CharacterBag2Slot,
    CharacterBag3Slot,
    MainMenuBarBackpackButton
}

local _MICRO_BUTTONS = {
    CharacterMicroButton,
    SpellbookMicroButton,
    TalentMicroButton,
    AchievementMicroButton,
    QuestLogMicroButton,
    GuildMicroButton,
    LFDMicroButton,
    CollectionsMicroButton,
    EJMicroButton,
    StoreMicroButton,
    MainMenuMicroButton
}

local _ADD_VIS = {
    [Visibility.SHOW] = function(frame) frame:Show() end,
    [Visibility.HIDE] = function(frame) HideFrame:Add(frame) end,
    [Visibility.HOVER] = function(frame) HoverAction.Setup(frame, 2) end,
    [Visibility.COMBAT] = function(frame) CombatShowFrame:AddFrame(frame) end
}

local _REMOVE_VIS = {
    [Visibility.SHOW] = function(frame) end,
    [Visibility.HIDE] = function(frame) HideFrame:Remove(frame) end,
    [Visibility.HOVER] = function(frame) HoverAction.Remove(frame, 2) end,
    [Visibility.COMBAT] = function(frame) CombatShowFrame:RemoveFrame(frame) end
}

-- Private Methods
local function _ZoomButtons(zoom)
    Table.ForEach(_BAG_BUTTONS, function(button)
        ZoomAction.Button(button, zoom)
    end)
end

local function _CreateFrame()
    local frame = CreateFrame("Frame",
                              "BaikArtFrame",
                              UIParent,
                              "SecureFrameTemplate")
    frame:SetFrameStrata("LOW")
    frame:SetSize(298, 88)
    frame:SetToplevel(true)
    frame:EnableMouse(true)
    frame:SetPoint("BOTTOMRIGHT")
    MicroButtonAndBagsBar:SetParent(frame)
    MicroButtonAndBagsBar:ClearAllPoints()
    MicroButtonAndBagsBar:SetPoint("BOTTOMRIGHT", "UIParent", "BOTTOMRIGHT")

    Table.ForEach(_MICRO_BUTTONS, function(button)
        button:SetParent(MicroButtonAndBagsBar)
    end)

    return frame
end

-- Private API
function MicroButtonAndBagActionBar:_SetVisibility(visibility)
    local frame = self._frame
    local last_visibility = self._visibility

    if last_visibility then
        _REMOVE_VIS[last_visibility](frame)
    end

    _ADD_VIS[visibility](frame)
    self._visibility = visibility
end

-- Ace Callbacks
function MicroButtonAndBagActionBar:OnInitialize()
    self._frame = _CreateFrame()
    BagAction.Zoom:Event():Register(_ZoomButtons)
    BagAction.Visibility:Event():Register(self, MicroButtonAndBagActionBar._SetVisibility)
    Log:i("MicroButtonAndBagActionBar OnInitialize")
end

function MicroButtonAndBagActionBar:OnEnable()
    Log:i("MicroButtonAndBagActionBar OnEnable")
end

function MicroButtonAndBagActionBar:OnDisable()
    Log:i("MicroButtonAndBagActionBar OnDisable")
end

-- Export Module
BaikActionBar.Frame.MicroButtonAndBagActionBar = MicroButtonAndBagActionBar
