-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Frame = Baik.Base.Frame
local Log = Baik.Base.Log
local Table = Baik.Base.Table
local ZoomAction = Baik.Action.ZoomAction

local BaikActionBar = _G.BaikActionBar
local ActionBarAction = BaikActionBar.Action.ActionBarAction

-- Constants
local _BUTTONS = Frame.FromPattern("MultiBarRightButton%d",
                                   NUM_MULTIBAR_BUTTONS)

-- Create Module
local RightActionBar = BaikActionBar:NewModule("RightActionBar")

-- Private Methods
local function _Zoom(zoom)
    Table.ForEach(_BUTTONS, function(button)
        ZoomAction.Button(button, zoom)
    end)
end

local function _ExtraButton(extra)
    local _DEFAULT = {
        parent = MultiBarRight,
        args = { "TOP", MultiBarRightButton11, "BOTTOM", 0, -6 }
    }
    local _EXTRA = {
        parent = BaikBottomRightActionBarFrame,
        args = { "LEFT", MultiBarBottomRightButton12, "RIGHT", 6, 0 }
    }

    local value = extra and _EXTRA or _DEFAULT
    MultiBarRightButton12:SetParent(value.parent)
    MultiBarRightButton12:ClearAllPoints()
    MultiBarRightButton12:SetPoint(unpack(value.args))
end

-- Ace Callbacks
function RightActionBar:OnInitialize()
    MultiBarRightButton12:SetAttribute("actionpage", 3)
    ActionBarAction.Zoom:Event():Register(_Zoom)
    ActionBarAction.Extra:Event():Register(_ExtraButton)
    Log:i("RightActionBar OnInitialize")
end

function RightActionBar:OnEnable()
    Log:i("RightActionBar OnEnable")
end

function RightActionBar:OnDisable()
    Log:i("RightActionBar OnDisable")
end
-- Export Module
BaikActionBar.Frame.RightActionBar = RightActionBar
