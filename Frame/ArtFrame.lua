-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local CombatShowFrame = Baik.Frame.CombatShowFrame
local HideFrame = Baik.Frame.HideFrame
local HoverAction = Baik.Action.HoverAction
local Log = Baik.Base.Log
local Visibility = Baik.Data.Visibility

local BaikActionBar = _G.BaikActionBar
local ActionBarAction = BaikActionBar.Action.ActionBarAction
local StatusBarEvent = BaikActionBar.Event.StatusBarEvent

-- Create Module
local ArtFrame = Baik:NewModule("ArtFrame")

-- Constants
local _ADD_VIS = {
    [Visibility.SHOW] = function(frame) frame:Show() end,
    [Visibility.HIDE] = function(frame) HideFrame:Add(frame) end,
    [Visibility.HOVER] = function(frame) HoverAction.Setup(frame, 2) end,
    [Visibility.COMBAT] = function(frame) CombatShowFrame:AddFrame(frame) end
}

local _REMOVE_VIS = {
    [Visibility.SHOW] = function(frame) end,
    [Visibility.HIDE] = function(frame) HideFrame:Remove(frame) end,
    [Visibility.HOVER] = function(frame) HoverAction.Remove(frame, 2) end,
    [Visibility.COMBAT] = function(frame) CombatShowFrame:RemoveFrame(frame) end
}

-- Private Methods
local function CreateMainMenuFrame()
    local frame = CreateFrame("Frame",
                              "BaikArtFrame",
                              UIParent,
                              "SecureHandlerStateTemplate")
    frame:SetFrameStrata("LOW")
    frame:SetSize(550, 49)
    frame:SetPoint("BOTTOM", 18, 0)
    frame:SetAttribute("_onstate-visibility",
    [=[
        local hide = newstate == "hide"
        if hide then
            self:Hide()
        else
            self:Show()
        end
    ]=])
    RegisterStateDriver(frame, "visibility", "[petbattle][overridebar] hide; show")

    -- Setup page number
    local page_number = frame:CreateFontString("BaikPageNumber",
                                               "BACKGROUND",
                                               "GameFontNormalSmall")
    page_number:SetText("b")
    page_number:SetPoint("RIGHT", -5, -3)

    -- Setup background
    local bg_texture = frame:CreateTexture("BaikMainMenuBackground",
                                           "BACKGROUND")
    bg_texture:SetAtlas("hud-MainMenuBar-small", true)
    bg_texture:SetPoint("BOTTOM")

    -- Setup Gryphons
    local left_texture = frame:CreateTexture("BaikLeftGryphon",
                                             "BACKGROUND")
    left_texture:SetAtlas("hud-MainMenuBar-gryphon", true)
    left_texture:SetPoint("BOTTOMLEFT", -98, 0)

    local right_texture = frame:CreateTexture("BaikRightGryphon",
                                             "BACKGROUND")
    right_texture:SetAtlas("hud-MainMenuBar-gryphon", true)
    right_texture:SetPoint("BOTTOMRIGHT", 98, 0)
    right_texture:SetTexCoord(1, 0, 0, 1)

    return frame
end

local function _ShowGryphon(show)
    if show then
        BaikLeftGryphon:Show()
        BaikRightGryphon:Show()
        return
    end

    BaikLeftGryphon:Hide()
    BaikRightGryphon:Hide()
end

local function _ShowStatus(status)
    local y_offset = status and 12 or 0
    BaikArtFrame:SetPoint("BOTTOM", 18, y_offset)
    BaikLeftGryphon:SetPoint("BOTTOMLEFT", -98, -y_offset)
    BaikRightGryphon:SetPoint("BOTTOMRIGHT", 98, -y_offset)
end

-- Public API
function ArtFrame:_SetVisibility(visibility)
    local frame = self._frame
    local last_visibility = self._visibility

    if last_visibility then
        _REMOVE_VIS[last_visibility](frame)
    end

    _ADD_VIS[visibility](frame)
    self._visibility = visibility
end

-- Ace Callbacks
function ArtFrame:OnInitialize()
    self._frame = CreateMainMenuFrame()
    ActionBarAction.Gryphon:Event():Register(_ShowGryphon)
    StatusBarEvent.Visibility:Register(_ShowStatus)
    ActionBarAction.Visibility:Event():Register(self, ArtFrame._SetVisibility)
    Log:i("ArtFrame OnInitialize")
end

function ArtFrame:OnEnable()
    Log:i("ArtFrame OnEnable")
end

function ArtFrame:OnDisable()
    Log:i("ArtFrame OnDisable")
end

-- Export Module
Baik.Frame.ArtFrame = ArtFrame
