-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local GameEvent = Baik.Event.GameEvent
local RepositoryEvent = Baik.Event.RepositoryEvent
local Table = Baik.Base.Table
local ZoomAction = Baik.Action.ZoomAction

local BaikActionBar = _G.BaikActionBar
local ActionBarAction = BaikActionBar.Action.ActionBarAction

-- Create Module
local ActionBar = Class()

-- Constants
local _BUTTON_SPACE = 6

-- Private Methods
local function _CreateBar(parent, format, max_buttons)
    local buttons = {}

    -- Initial anchor values
    local point = "LEFT"
    local anchor = parent
    local x_offset = 0
    for idx = 1, max_buttons, 1 do
        local name = string.format(format, idx)
        local button = _G[name]
        Assert.NotNull(button)

        -- Set Button Layout
        button:SetParent(parent)
        button:ClearAllPoints()
        button:SetPoint("LEFT", anchor, point, x_offset, 0)
        -- Force buttons to update on page change
        button:SetAttribute("_childupdate-page", [=[
            self:SetAttribute("page", message)
        ]=])

        -- Update anchor valus
        point = "RIGHT"
        anchor = button
        x_offset = _BUTTON_SPACE

        buttons[idx] = button
    end

    return buttons
end

-- Private API
function ActionBar:_Zoom(zoom)
    Table.ForEach(self._buttons, function(button)
        ZoomAction.Button(button, zoom)
    end)
end

-- Public API
function ActionBar:New(parent, format, max_buttons)
    Assert.String(format)
    Assert.Table(parent)
    Assert.Number(max_buttons)

    local obj = Class(self)
    obj._buttons = _CreateBar(parent, format, max_buttons)

    return obj
end

function ActionBar:SetupEvents()
    ActionBarAction.Zoom:Event():Register(self, ActionBar._Zoom)
end

-- Export Module
BaikActionBar.Frame.ActionBar = ActionBar
