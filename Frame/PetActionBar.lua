-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Log = Baik.Base.Log

local BaikActionBar = _G.BaikActionBar
local ActionBarAction = BaikActionBar.Action.ActionBarAction
local ActionBar = BaikActionBar.Frame.ActionBar
local SecondaryActionBarFrame = BaikActionBar.Frame.SecondaryActionBarFrame

-- Create Module
local PetActionBar = BaikActionBar:NewModule("PetActionBar")

-- Constants
local _FORMAT = "PetActionButton%d"

-- Private API
local function _CreateBarFrame()
    local secondary_bar = SecondaryActionBarFrame.New("BaikPetActionBarFrame",
                                                      BaikArtFrame)
    local frame = secondary_bar:Get()
    frame:SetAttribute("_onstate-pet",
    [=[
        local show = newstate == "show"
        if show then
            self:Show()
        else
            self:Hide()
        end
    ]=])
    RegisterStateDriver(frame, "pet", "[pet, nopossessbar, novehicleui] show; hide")

    return secondary_bar
end

-- Ace Callbacks
function PetActionBar:OnInitialize()
    local frame = _CreateBarFrame()
    self._action_bar = ActionBar:New(frame:Get(),
                                     _FORMAT,
                                     NUM_PET_ACTION_SLOTS)
    self._frame = frame
    self._frame:SetupEvents()
    self._action_bar:SetupEvents()
    Log:i("PetActionBar OnInitialize")
end

function PetActionBar:OnEnable()
    Log:i("PetActionBar OnEnable")
end

function PetActionBar:OnDisable()
    Log:i("PetActionBar OnDisable")
end

-- Export Module
BaikActionBar.Frame.PetActionBar = PetActionBar
