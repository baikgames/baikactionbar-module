-- Load Module
local Baik = _G.Baik
local Class = Baik.Base.Class
local Table = Baik.Base.Table

local BaikActionBar = _G.BaikActionBar
local ActionBar = BaikActionBar.Frame.ActionBar
local ActionBarAction = BaikActionBar.Action.ActionBarAction

-- Create Module
local GridActionBar = Class(ActionBar)

-- Public API
function GridActionBar:ShowGrid()
    if InCombatLockdown() then
        return
    end

    local buttons = self._buttons
    Table.ForEach(buttons, function(button)
        button:SetAttribute("showgrid", 1)
        button:Show()
    end)
end

function GridActionBar:HideGrid()
    if InCombatLockdown() then
        return
    end

    local buttons = self._buttons
    Table.ForEach(buttons, function(button)
        if not HasAction(button.action) then
            button:SetAttribute("showgrid", 0)
            button:Hide()
        end
    end)
end

-- Export Module
BaikActionBar.Frame.GridActionBar = GridActionBar
