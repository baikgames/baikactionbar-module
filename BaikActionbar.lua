-- Load Module
local _G = _G
local Baik = _G.Baik
local Log = Baik.Base.Log

-- Create Module
local BaikActionBar = Baik:NewModule("BaikActionBar")

-- Ace Callbacks
function BaikActionBar:OnInitialize()
    Log:i("BaikActionBar OnInitialize")
end

function BaikActionBar:OnEnable()
    Log:i("BaikActionBar OnEnable")
end

function BaikActionBar:OnDisable()
    Log:i("BaikActionBar OnDisable")
end

-- Export Module
_G.BaikActionBar = BaikActionBar
