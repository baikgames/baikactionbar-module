-- Load Module
local Baik = _G.Baik
local Category = Baik.Option.Category
local Option = Baik.Option.Option
local ToggleArg = Baik.Option.Arg.ToggleArg
local VisibilityArg = Baik.Option.Arg.VisibilityArg

local BaikActionBar = _G.BaikActionBar
ActionBarAction = BaikActionBar.Action.ActionBarAction
local ActionBarCategory = Baik.Option.ActionBarCategory

-- Create Module
local GeneralCategory = Category.New("General", "general", Category.TREE, 0)

-- Constants
local _ARGS = {
    ToggleArg.New("Zoom", "zoom", ActionBarAction.Zoom, 0, ToggleArg.WIDTH_FULL),
    ToggleArg.New("Gryphon", "gryphon", ActionBarAction.Gryphon, 1, ToggleArg.WIDTH_FULL),
    ToggleArg.New("Extra", "extra", ActionBarAction.Extra, 2, ToggleArg.WIDTH_FULL),
    VisibilityArg.New("Visibility", "visibility", ActionBarAction.Visibility, 3, VisibilityArg.WIDTH_NORMAL)
}

-- Setup Module
GeneralCategory:AddAll(_ARGS)
ActionBarCategory:Add(GeneralCategory)

-- Export Module
Baik.Option.GeneralCategory = GeneralCategory
