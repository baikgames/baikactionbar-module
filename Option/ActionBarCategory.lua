-- Load Module
local Baik = _G.Baik
local Category = Baik.Option.Category
local Option = Baik.Option.Option
local ToggleArg = Baik.Option.Arg.ToggleArg

local BaikActionBar = _G.BaikActionBar
ActionBarAction = BaikActionBar.Action.ActionBarAction

-- Create Module
local ActionBarCategory = Category.New("ActionBar", "action_bar", Category.TAB, 1)

-- Setup Module
Option:Category():Add(ActionBarCategory)

-- Export Module
Baik.Option.ActionBarCategory = ActionBarCategory
