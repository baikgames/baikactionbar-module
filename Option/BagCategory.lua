-- Load Module
local Baik = _G.Baik
local Category = Baik.Option.Category
local Option = Baik.Option.Option
local ToggleArg = Baik.Option.Arg.ToggleArg
local VisibilityArg = Baik.Option.Arg.VisibilityArg

local BaikActionBar = _G.BaikActionBar
local BagAction = BaikActionBar.Action.BagAction
local ActionBarCategory = Baik.Option.ActionBarCategory

-- Create Module
local BagCategory = Category.New("Bag", "bag", Category.TREE, 1)

-- Constants
local _ARGS = {
    ToggleArg.New("Zoom", "zoom", BagAction.Zoom, 0, ToggleArg.WIDTH_FULL),
    VisibilityArg.New("Visibility", "visibility", BagAction.Visibility, 1, VisibilityArg.WIDTH_NORMAL)
}

-- Setup Module
BagCategory:AddAll(_ARGS)
ActionBarCategory:Add(BagCategory)

-- Export Module
Baik.Option.BagCategory = BagCategory
