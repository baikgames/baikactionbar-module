-- Load Module
local Baik = _G.Baik
local Category = Baik.Option.Category
local Option = Baik.Option.Option
local ToggleArg = Baik.Option.Arg.ToggleArg

local BaikActionBar = _G.BaikActionBar
local StatusBarAction = BaikActionBar.Action.StatusBarAction
local ActionBarCategory = Baik.Option.ActionBarCategory

-- Create Module
local StatusCategory = Category.New("Status", "status_bar", Category.TREE, 2)

-- Constants
local _ARGS = {
    ToggleArg.New("Xp", "xp", StatusBarAction.Xp, 0, ToggleArg.WIDTH_FULL),
}

-- Setup Module
StatusCategory:AddAll(_ARGS)
ActionBarCategory:Add(StatusCategory)

-- Export Module
Baik.Option.StatusCategory = StatusCategory
